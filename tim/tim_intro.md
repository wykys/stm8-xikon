# Čítače / Časovače - Úvod

Čitače / časovače jsou v mikroprocesorové technice po vstupech a výstupech nejpoužívanější periferií. Umožňují nám provádět relativně přesné časování, měření i generování pulzů. Jejich přesnost vychází s přesnosti frekvence hodinového signálu.

## Předpoklady

Téma čítačů je poměrně obsáhlé, na to že v základu jde o inkrementování hodnoty čítačího registru. Před zahájením studia této kapitoly by bylo vhodné mít základní znalosti čítačů s číslicové techniky.

### Dělička frekvence

Základní stavební kámen asynchronního čítače je dělička frekvence.

![freq div 1](data/counter-cou1.png)

### Asynchronní čítač

V mikroprocesorové technice se používají složitější synchronní obvodová řešení čítačů, leč pro základní představu funkce nám dobře poslouží i jednoduchý asynchronní čítač, složený s děliček frekvence dvěma.

![freq div 2](data/counter-cou2.png)

## Typy čítání

Čítače obvykle pracují v jednom s níže popsaných režimů:

### Čítání nahoru

Čítač čítá od nuly do stropu (hodnota příslušného `ARR` registru). Po dosažení stropu čítač přeteče a tedy opět čítá od nuly.

![up couting](data/up-counting.png)

### Čítání dolů

Čítač čítá od stropu (hodnota `ARR` registru) do nuly. Po dosažení nuly dochází k podtečení opět na hodnotu stropu.

![down couting](data/down-counting.png)

### Čítání nahoru a dolů

Čítač čítá od nuly do stropu (hodnota `ARR` registru), tedy směrem nahoru. Po dosažení stropu se směr čítání obrátí a čítač začne čítat směrem dolů. Když čítač dočítá do nuly, proces se opět opakuje od začátku. Tento režim má využití zejména při generování fázově korektního `PWM`.

![up-down couting](data/center-aligned-counting.png)

## Zdroj hodinového signálu

Čítač čítá pulzy. Proto potřebuje na vstup přivést hodinový signál. Na obrázku níže je znázorněno blokové schéma hodinového systému `STM8`.

Červeně je zvýrazněno generování hodinového signálu pro čítače. Této konfigurace je možné dosáhnout následovně:

```c
// nastavení master frekvence na 16 MHz s HSI RC oscilátoru
CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
```

![clk](data/clk.png)

## TIM4 8-bit basic timer

Tento čítač spadá do kategorie basic timers (základní časovače). Umí jen čítat nahoru do nastavitelného stropu a vyvolat přerušení při přetečení. Blokově jej znázorňuje následující obrázek.

![TIM4](data/tim4.png)

### TIM4 hlavní funkce

* __8b__ čítač nahoru s nastavitelným stropem (auro reload registrem `ARR`)
* Programovatelná __3b__ předdělička hodinové frekvence, která může dělit: 1, 2, 4, 8, 16, 32, 64, 128
* Generování přerušení:
  * Při přetečení čítače (counter update: counter overflow)

## Změna stropu čítače

Strop čítače je nastavitelný a tedy se dá v programu měnit. Hodnota stropu čítače je uložena v `ARR` registru. Každý čítač má svůj.

K nastavení stropu čítače (zápisu hodnoty do `ARR` registru) slouží tato funkce:

```c
void TIM4_SetAutoreload(uint8_t Autoreload);
```

Pokud do tohoto registru zapíšeme novou hodnotu může ale i nemusí se projevit okamžitě. To kdy se nová hodnota začne používat záleží na konfiguraci preload.

### Bez přednadčítání stropu

Pokud je preload deaktivován, hodnota je ihned po zápisu do registru použita jako noví trop.

```c
TIM4_ARRPreloadConfig(DISABLE);
```

![not preloaded](data/not-preloaded.png)

### Přednačítání stropu

Pokud je preload aktivní, nová hodnota stropu se aktualizuje až po přetečení čítače.

```c
TIM4_ARRPreloadConfig(ENABLE);
```

![preloaded](data/preloaded.png)

## Výpočet frekvence přetékání čítače

```math
f_o = \frac{f_{\texttt{MASTER}}}{P \cdot (C + 1)}
```

## Ukázka použití TIM4

Napište program, který bude togglovat `PC5`. Pin bude togglovat jednou za `2 ms`. Použijte HSI oscilátor.

1. Jakou frekvencí bude togglovat LED?
2. Jakou použijeme předděličku?
3. Jaký bude strop čítače?

```math
T = 2~ms
\Rightarrow
f = \frac{1}{T} = \frac{1}{2 \cdot 10^{-3}} = 500~Hz
```

<details>
<summary>Řešení 1</summary>

```math
T_{toggle} = 2 \cdot 2~ms = 4~ms
\Rightarrow
f_{toggle} = \frac{1}{4 \cdot 10^{-3}} = \underline{\underline{250~Hz}}
```

</details>

<details>
<summary>Řešení 2</summary>

Pro výpočet zatím neznám hodnotu stropu, proto si prozatím zvolím za `C` nejvyšší hodnotu čítače, ta je u 8b čítače $`2^8 - 1`$ tedy $`255`$.

Ve vztahu se nám objeví $`-1 + 1`$, takže by stačilo psát $`2^8`$.

```math
P = \frac{f_{\texttt{MASTER}}}{f_o \cdot (C + 1)} = \frac{16 \cdot 10^6}{500 \cdot \left(2^8 - 1 + 1 \right)} = 125
```

Předdělička u `TIM4` musí být mocnina dvojky, proto použiju nejbližší vyšší mocninu. Pokud by byla použita nižší mocnina, už bi mi nestačilo `8b` na hodnotu stropu.

```math
P = 125 \Rightarrow P = \underline{\underline{128}}
```

</details>

<details>
<summary>Řešení 3</summary>

```math
C = \frac{f_{\texttt{MASTER}}}{f_o \cdot P} - 1 = \frac{16 \cdot 10^6}{500 \cdot 128} - 1 = \underline{\underline{249}}
```

</details>

<details>
<summary>Zdroják</summary>

```c
#include "stm8s.h"

void main(void)
{
    // nastavení hodin na 16 MHz
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
    // PC5 nastaven jako výstup
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
    // inicializace časovače
    TIM4_TimeBaseInit(TIM4_PRESCALER_128, 249);
    // spuštění časovače
    TIM4_Cmd(ENABLE);

    while(1)
    {
        while (TIM4_GetFlagStatus(TIM4_FLAG_UPDATE) != SET)
            ;
        TIM4_ClearFlag(TIM4_FLAG_UPDATE);
        GPIO_WriteReverse(GPIOC, GPIO_PIN_5);
    }
}
```

</details>

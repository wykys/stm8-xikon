# Čítače / Časovače - PWM

Pulzně šířková modulace neboli PWM (Pulse Width Modulation), je jednoduchá modulace používaná například k přenosu dat (komunikace se servy), nebo například k regulaci výkonu (ovládání jasu LED, řízení otáček komutátorových motorů, spínané měniče).

`PWM` se dá samozřejmě generovat softwarově s využitím ovládání `GPIO`, to však zatěžuje jádro procesoru a může to i znemožnit provádět další operace. Tato kapitola se zabývá generování hardwarového `PWM` pomocí kanálu čítače, tím pádem bez zatěžování jádra procesoru.

## Předpoklady

Tohle téma navazuje na základní poznatky s úvodu do čítačů.

## PWM

Signál PWM je analogový (spojitý v hodnotě i čase). PWM má konstantní periodu. Hodnoty signálu nabývají logické nuly nebo jedničky. V reálném světě je změna logické úrovně 0 ⟷ 1 spojitá, ideální PWM signál má nekonečnou strmost (rychlost změny hodnoty).

Hlavním parametrem PWM je __střída__ signálu (anglicky duty cycle), což je poměr doby, kdy je signál v logické jedničce, vůči periodě signálu. Tato veličina je často označována jako `D` (Duty) nebo `DCL` (Duty CycLe). Nejčastěji se vyjadřuje ve dvou reprezentacích: jako poměr, v procentech:

```math
D = \frac{T_H}{T}~[-];~~~~
D = \frac{T_H}{T} \cdot 100~[\%]
```

### Animace PWM

Ukázka různých stříd `PWM` signálu.

![PWM animace](data/pwm.gif)

### Jak funguje generování PWM?

Generování PWM založené na čítači v nejjednodušší formě funguje následovně. Čítač čítá nahoru a po dosažení hodnoty stropu přeteče a začíná od nuly. To na obrázku znázorňuje modrý průběh.

Červený signál je práh překlopení, tedy naše střída. Pokud je hodnota čítače (modrý průběh) nižší nebo rovna hodnotě prahu je výstup logická jednička, v opačném případě logická nula.

Zelený signál je výsledné PWM.


![PWM časové průběhy](data/pwm_generator_a.png)

## TIM2, TIM3 16-bit general purpose timers

Pro generování PWM je potřeba použít čítač, který má k dispozici kanál. Tedy už nebude stačit __basic timer__ jako `TIM4`, ale bude potřeba sáhnout po komplexnějším čítači. Tato kapitola používá __general purpose timer__ (`TIM2`, `TIM3`), ale šel by použít i __advance timer__ (`TIM1`).

### Blokové schéma general purpose timeru

Následuje obzázek s blokovým schématem, navíc od basic timeru z něm přibili 3 kanály.

![TIM4](data/tim2_tim3.png)

### TIM2/TIM3 hlavní funkce

* __16b__  čítač nahoru s nastavitelným stropem (auro reload registrem `ARR`)
* Programovatelná __4b__ předdělička hodinové frekvence, která může dělit mocninami dvojky v rozsahu: 1 až 32768
* 3 nezávislé kanály pro:
  * Input capture
  * Output compare
  * PWM generation (edge-aligned mode)
* Interrupt request generation on the following events:
  * Update: counter overflow, counter initialization (by software)
  * Input capture
  * Output compare

### Kanály časovačů (capture / compare)

Kanály mají vstupní část, do které je možné přivést vnější signál. Ta pro generování PWM je zbytná. Její využití je například při měření pulzů).

Pro generování PWM je důležitá výstupní část, jenž umožňuje s časovače vyvést výstup na pin mikrokontroléru.

Vstupní i výstupní část společně sdílí Capture/Compare registry, které při generování PWM budou uchovávat prahovou hodnotu.

![tim channels](data/tim_channels.png)

### Výstupní části kanálů

Detail výstupní části, kterou je možné fyzicky uvnitř MCU napojit na `GPIO`.

![Výstupní části kanálů](data/output_stage.png)

### Detail výstupního kanálu

Podrobné blokové schéma výstupní části jednoho z kanálů. V této části je blok mód, kterým se konfiguruje funkce výstupního kanálu, mi se zatím omezíme na mód PWM1. Dále je tu multiplexor, kterým se nastavuje, zda dojde k negaci signálu nebo ne. Dále je tu output enable, který zprostředkovává propojení na výstupní pin.

![Detail výstupního kanálu](data/output_stage_ch1.png)

## Ukázka použití TIM2 pro generování PWM

Ukázkový program generuje `PWM` signál pro servo a simultánně s tím bliká LED na `PC5`.

```c
#include "stm8s.h"

void main(void)
{
    // nastavení hodin na 16 MHz
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);

    // Inicializace GPIO pro LED
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

    // TIM2 CH2 -> PD3
    TIM2_TimeBaseInit(             // inicializace časovače TIM2
        TIM2_PRESCALER_8,          // předdělička
        39999                      // strop čítače
    );
    TIM2_OC2Init(                  // inicializace kanálu OC2
        TIM2_OCMODE_PWM1,          // funkce kanálu PWM1
        TIM2_OUTPUTSTATE_ENABLE,   // povolit HW výstup
        4000,                      // překlápěcí práh
        TIM2_OCPOLARITY_HIGH       // polarita výsledného signálu
    );
    TIM2_Cmd(ENABLE);              // spuštění čítače

    TIM2_OC2PreloadConfig(ENABLE); // povolení preload prahového registru
    TIM2_SetCompare2(2000);        // změna prahu

    // paralelně s generováním PWM můžeme dělat cokoliv jiného
    while (1)
    {
        GPIO_WriteReverse(GPIOC, GPIO_PIN_5);
        for (uint32_t i = 0; i < 100000; i++)
            ;
    }
}
```

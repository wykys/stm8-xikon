# Git

* [Verzování - Git](https://gitlab.com/wykys/stm8-xikon/-/tree/main/git)
  * [Úvod](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_intro.md)
  * [Větvení](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_vetve.md)
  * [Zásobník](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_zasobnik.md)
  * [Vzdálené servery](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_server.md)
  * [Ukazatel HEAD](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_head.md)

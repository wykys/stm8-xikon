# Zásobník - skrýš

Slouží pro odkládání rozpracovaných souborů.

## Vložení rozpracovaných souborů do zásobníku

```
git stash
```

## Vrácení souborů se zásobníku do složky

```
git stash apply
```

## Smazání zásobníku

```
git stash clear
```

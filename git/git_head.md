# Ukazatel HEAD

`HEAD` je ukazatel na obvykle poslední commit. Manipulací s tím, kam tento ukazatel ukazuje, můžeme zahodit již vytvořené commity.

![HEAF](data/head.png)

Někdy se může hodit zrušit poslední commit.

__Není dobré to provádět, pokud jste jiš pomocí `push` odeslali práci na server !!!__

Hrozí riziko, že někdo si již stáhl vaši práci a mohl by nastat konflikt...

## Návrat do stavu po commitu

Tento příkaz zruší všechny změny, které jsme provedly a projekt bude ve stavu, v jakém byl při posledním commitu.

```
git reset --hard HEAD
```

## Zrušení posledního commitu

Příkaz posune ukazatel `HEAD` o jeden commit dozadu.

```
git reset --hard HEAD^
```

## Zrušení posledních 2 commitů

Tento příkaz lze použít k zahození libovolného počtu provedených commitů. Za číslo 2 je možné dostat o kolik commitů chceme jít dozadu. Pokud bychom 2 nahradili za 1, bude to ekvivalentní předchozímu příkazu.

```
git reset --hard HEAD~2
```

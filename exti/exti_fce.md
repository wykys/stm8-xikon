# Externí přerušení - Knihovní funkce

V této kapitole je souhrn nejpoužívanějších funkcí pro práci s externím přerušením.

## Předpoklady

Tato kapitola předpokládá základy jazyka C.

### Inicializace GPIO pro EXTI

Funkce inicializuje `GPIO` jako vstupní, plovoucí nebo s pull-up odporem a povolí průchod signálu s tohoto `GPIO` do `EXTI` kontroléru.

```c
void GPIO_Init(
    GPIO_TypeDef* GPIOx,        // GPIO port
    GPIO_Pin_TypeDef GPIO_Pin,  // GPIO pin
    GPIO_Mode_TypeDef GPIO_Mode // GPIO mód
);
```

<details>
<summary>Ukázka použití</summary>

Ukázka inicializace `PE4` jako vstupní plovoucí pin pro `EXTI`:

```c
GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_IT);
```

Ukázka inicializace `PC3` jako vstupní pin pro `EXTI` s pull-up odporem:

```c
GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_IN_PU_IT);
```

</details>

### Nastavení citlivosti EXTI

Tato funkce nastavuje, na jakou událost na vstupu `GPIO` bude `EXTI` vyvoláno.

```c
void EXTI_SetExtIntSensitivity(
    EXTI_Port_TypeDef Port,                   // EXTI port
    EXTI_Sensitivity_TypeDef SensitivityValue // EXTI citlivost
);
```

<details>
<summary>Ukázka použití</summary>

Ukázky nastavení všech možných citlivostí pro `Port E`:

Citlivost na sestupnou hranu a logickou nulu.

```c
EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_FALL_LOW);
```

Citlivost na náběžnou hranu.

```c
EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_RISE_ONLY);
```

Citlivost na sestupnou hranu.

```c
EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_FALL_ONLY);
```

Citlivost na náběžnou i sestupnou hranu.

```c
EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_RISE_FALL);
```

</details>

### Nastavení priority přerušení

```c
void ITC_SetSoftwarePriority(
    ITC_Irq_TypeDef IrqNum,                 // zdroj přerušení
    ITC_PriorityLevel_TypeDef PriorityValue // priorita
);
```

<details>
<summary>Ukázka použití</summary>

```c
// nastavení priority přerušení EXTI portu E na 1
ITC_SetSoftwarePriority(EXTI_PORT_GPIOE, ITC_PRIORITYLEVEL_1);
```

</details>

### Globální povolení přerušení

```c
#define enableInterrupts()  __asm__("rim")
```

<details>
<summary>Ukázka použití</summary>

```c
// globální povolení přerušení
enableInterrupts();
```

</details>

### Globální zakázání přerušení

```c
#define disableInterrupts() __asm__("sim")
```

<details>
<summary>Ukázka použití</summary>

```c
// globální zakázání přerušení
disableInterrupts();
```

</details>

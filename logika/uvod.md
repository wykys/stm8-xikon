# Logika

Naprostý úvod.

## Úkrok stranou

Jak jednoduše dělat pravdivostní tabulky, aneb převody mezi binární a dekadickou soustavou.

$$
y = \sum_{i=0}^N b_i \cdot 2^i
$$

- Kde:
  - $N$ je počet bitů $-1$
  - $y$ je výslední číslo v dekadické soustavě
  - $i$ je číslo bitu indexované od 0 a z prava

### Příklad - dvojková do dekadické

Když budeme mít například binární číslo $(1011)_2$ a budeme ho chtít převést do dekadické soustavy.

$$
\begin{align}
y &= (1011)_2 \nonumber\\
y &= \sum_{i=0}^{4-1} b_i \cdot 2^i = \sum_{i=0}^3 b_i \cdot 2^i \nonumber\\
y &= 1 \cdot 2^0 + 1 \cdot 2^1 + 0 \cdot 2^2 + 1 \cdot 2^3 \nonumber\\
y &= 1 \cdot 1 + 1 \cdot 2 + 0 \cdot 4 + 1 \cdot 8 \nonumber\\
y &= 1 + 2 + 8 \nonumber\\
y &= \underline{\underline{(11)_{10}}} \nonumber
\end{align}
$$

### Jak to využít při sestavování pravdivostních tabulek?

- V tabulce je potřeba pokrýt všechny stavy a toho lze snadno docílit pomocí číselné řady od 0 do počtu stavů `-1`.
- Jak zjistit počet stavů?
  - Vezme se počet vstupních proměnných, které mohou nabývat hodnot 0 (nepravda) nebo 1 (pravda) a tento počet se umocní na druhou.
  - Pokud budu mít vstupní proměnné A a B, bude počet stavů $2^2 = 4$
  - Pokud budu mít vstupní proměnné A, B a C, bude počet stavů $2^3 = 8$

### Ukázka pravdivostní tabulky pro proměnnou A

Počet stavů: $2^1 = 2$

| STAV  |   A   |
| :---: | :---: |
|   0   |   0   |
|   1   |   1   |

### Ukázka pravdivostní tabulky pro proměnnou A a B

Počet stavů: $2^2 = 4$

| STAV  |   A   |   B   |
| :---: | :---: | :---: |
|   0   |   0   |   0   |
|   1   |   0   |   1   |
|   2   |   1   |   0   |
|   3   |   1   |   1   |

### Ukázka pravdivostní tabulky pro proměnnou A, B a C

Počet stavů: $2^3 = 8$

| STAV  |   A   |   B   |   C   |
| :---: | :---: | :---: | :---: |
|   0   |   0   |   0   |   0   |
|   1   |   0   |   0   |   1   |
|   2   |   0   |   1   |   0   |
|   3   |   0   |   1   |   1   |
|   4   |   1   |   0   |   0   |
|   5   |   1   |   0   |   1   |
|   6   |   1   |   1   |   0   |
|   7   |   1   |   1   |   1   |

### Co mají tyto tabulky společného?

- Každý řádek představuje binární reprezentaci čísla stavu
- Na nejnižším bitu se mění jeho hodnota na každém řádku
- Z každým dalším bitem klesá frekvence jeho měnění na polovinu

---

## Logické operace

### `NOT`

- Jde o obrácení tvrzení
  - Z nepravdy pravdu
  - Z pravdy dělá nepravdu

- Lidsky

  - "A", případně "A zároveň"
  - Pravda pokud jsou bě tvrzení pravdivá

- Matematicky
  - Název: Negace
  - Symbol: $\neg$

- Programátorsky v jazyku C
  - Bitová negace: `~`
  - Logická negace: `!`

#### Různé "matematické" zápisy

$Y = \overline{A} = \neg A$

#### Pravdivostní tabulka

| STAV  |   A   |   Y   |
| :---: | :---: | :---: |
|   0   |   0   | __1__ |
|   1   |   1   | __0__ |

#### Ukázkový kód

```c
/* v Céčku */
if ( !chlastam_jako_duha() )
{
    printf("Jsem abstinent\n");
}
```

```python
# v Pythonu
if not chlastam_jako_duha():
    print("Jsem abstinent");
```

```javascript
// v JavaScriptu
if (!chlastamJakoDuha()) {
    console.log("Jsem abstinent");
}
```

### `AND`

- Jde o ekvivalent násobení
  - Někdy se značí: $\cdot$

- Lidsky

  - "A", případně "A zároveň"
  - Pravda pokud jsou bě tvrzení pravdivá

- Matematicky
  - Název: Konjunkce
  - Symbol: $\wedge$

- Programátorsky v jazyku C
  - Bitový součin: `&`
  - Logický součin: `&&`

#### Různé "matematické" zápisy

$Y = AB = A \cdot B = A \wedge B$

#### Pravdivostní tabulka

| STAV  |   A   |   B   |   Y   |
| :---: | :---: | :---: | :---: |
|   0   |   0   |   0   | __0__ |
|   1   |   0   |   1   | __0__ |
|   2   |   1   |   0   | __0__ |
|   3   |   1   |   1   | __1__ |

#### Ukázkový kód

```c
/* v Céčku */
if ( libi_se_mi_holky() && libi_se_mi_kluci() )
{
    printf("Asi budu bi\n");
}
```

```python
# v Pythonu
if libi_se_mi_holky() and libi_se_mi_kluci():
    print("Asi budu bi");
```

```javascript
// v JavaScriptu
if (libiSeMiHolky() && libiSeMiKluci()) {
    console.log("Asi budu bi");
}
```

### `OR`

- Jde o ekvivalent sčítání
  - Někdy se značí: $+$

- Lidsky

  - Nebo
  - Pravda pokud je aspoň jendo tvrzení pravdivé

- Matematicky
  - Název: Disjunkce
  - Symbol: $\vee$

- Programátorsky v jazyku C
  - Bitový součet: `|`
  - Logický součet: `||`

#### Různé "matematické" zápisy

$Y = A + B = A \vee B$

#### Pravdivostní tabulka

| STAV  |   A   |   B   |   Y   |
| :---: | :---: | :---: | :---: |
|   0   |   0   |   0   | __0__ |
|   1   |   0   |   1   | __1__ |
|   2   |   1   |   0   | __1__ |
|   3   |   1   |   1   | __1__ |

#### Ukázkový kód

```c
/* v Céčku */
if ( v_kuchyni_je_drak() || na_vcerejsek_mam_okno() )
{
    printf("Byl to fajn mejdan\n");
}
```

```python
# v Pythonu
if v_kuchyni_je_drak() or na_vcerejsek_mam_okno():
    print("Byl to fajn mejdan");
```

```javascript
// v JavaScriptu
if (vKuchyniJeDrak() | naVcerejsekMamOkno()) {
    console.log("Byl to fajn mejdan");
}
```
---

## Příklad

Je tenhle výraz `!(p | q)` svým chováním stejný jako tento `(!p) & (!q)`?

| STAV  |   p   |   q   | p  \| q | !(p \| q) |  !p   |  !q   | (!p) & (!q) |
| :---: | :---: | :---: | :-----: | :-------: | :---: | :---: | :---------: |
|   0   |   0   |   0   |    0    |   __1__   |   1   |   1   |    __1__    |
|   1   |   0   |   1   |    1    |   __0__   |   1   |   0   |    __0__    |
|   2   |   1   |   0   |    1    |   __0__   |   0   |   1   |    __0__    |
|   3   |   1   |   1   |    1    |   __0__   |   0   |   0   |    __0__    |

Jen jiná forma zápisu:

Je tenhle výraz $\overline{p + q}$ svým chováním stejný jako tento $\overline{p} \cdot \overline{q}$?

| STAV  |  $p$  |  $q$  | $p + q$ | $\overline{p + q}$ | $\overline{p}$ | $\overline{q}$ | $\overline{p} \cdot \overline{q}$ |
| :---: | :---: | :---: | :-----: | :----------------: | :------------: | :------------: | :-------------------------------: |
|   0   |   0   |   0   |    0    |       __1__        |       1        |       1        |               __1__               |
|   1   |   0   |   1   |    1    |       __0__        |       1        |       0        |               __0__               |
|   2   |   1   |   0   |    1    |       __0__        |       0        |       1        |               __0__               |
|   3   |   1   |   1   |    1    |       __0__        |       0        |       0        |               __0__               |

Takže __jo__ výrazy vyjadřují to stejné!

__Jo a není to náhoda je to důsledek De Morganovích zákonů!__
